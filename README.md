# hyper-components

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/hyper-components.svg)](https://www.npmjs.com/package/hyper-components) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save hyper-components
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'hyper-components'
import 'hyper-components/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
