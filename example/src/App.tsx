import React from 'react';

import { Button } from 'hyper-components';
import 'hyper-components/dist/index.css';

const App = () => {
  return (
    <Button size="lg" color="primary" disabled>
      Hello
    </Button>
  );
};

export default App;
