import * as React from 'react';
import { Button as RButton, ButtonProps } from 'reactstrap';

const Button: React.FC<ButtonProps> = ({ children, ...props }) => {
  return <RButton {...props}>{children || 'Button'}</RButton>;
};

export default Button;
