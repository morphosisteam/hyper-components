import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import pretty from 'pretty';

import Button from './Button';

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('renders with or without a name', () => {
  act(() => {
    render(<Button />, container);
  });
  expect(container.textContent).toBe('Button');

  act(() => {
    render(
      <Button size="lg" color="primary" disabled>
        Hello
      </Button>,
      container
    );
  });
  expect(container.textContent).toBe('Hello');
  expect(container.querySelector('button').className).toContain('btn-primary');
  expect(container.querySelector('button').disabled).toBe(true);
  expect(pretty(container.innerHTML)).toMatchInlineSnapshot(
    `"<button class=\\"btn btn-primary btn-lg disabled\\" disabled=\\"\\">Hello</button>"`
  ); /* ... gets filled automatically by jest ... */
});
